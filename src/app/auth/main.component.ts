import { Component, OnInit } from '@angular/core';
import { UserService } from '../core/user.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  constructor(
    private user: UserService
  ) { }

  async ngOnInit() {
    const user = await this.user.getUserInfo().toPromise();

    console.log(user);

    //   $(function () {
    //     $(window).hashchange(function () {
    //       var hash = location.hash.replace(/^#/, '');
    //       $(".panel").addClass("hide");
    //       if ($("#" + hash).length == 0)
    //         hash = "tologin";
    //       $("#" + hash).each(function (index, item) {
    //         $(item).addClass("fadeInLeft").removeClass("hide");
    //         $(item).find(".focused").focus();
    //       });
    //       $(window).hashchange();
    //     });

    //     $("#toactivate").each(function (index, target) {
    //       target = $(target);
    //       target.find(".resent-switch a").click(function () {
    //         target.find(".resent-switch").addClass("hide");
    //         target.find(".resent-captcha,.resent-submit").removeClass("fadeOutUp").addClass("fadeInDown").removeClass("hide");
    //         target.find(".resent-captcha input").focus();
    //       });
    //       target.find(".resent-submit").attr("href", "javascript:void(0);").click(function () {
    //         var captcha = target.find(".resent-captcha input").val();
    //         target.find(".resent-switch").addClass("success").removeClass("hide");
    //         target.find(".resent-switch .help-inline").text("啟動信已寄出。");
    //         target.find(".resent-switch a").text("重寄");
    //         target.find(".resent-captcha,.resent-submit").removeClass("fadeInDown").addClass("fadeOutUp");
    //         setTimeout(function () { target.find(".resent-captcha,.resent-submit").addClass("hide"); }, 400);
    //       });
    //     });
    //     $(".my-LoginQQ").click(function () {
    //       $("#toregister").find(".specialInfo").empty().append($('<p multi-lang-text="建立QQ連結帳號說明"><a href="#tologin" multi-lang-text="登入"></a></p>'));
    //       location.assign("#toregister");
    //     });

    //   });
    // function showModal(name) {

    //   $(name).modal({ backdrop: 'static', keyboard: false });
    // }
  }

}
