import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GadgetRoutingModule } from './gadget-routing.module';
import { ArrangeComponent } from './arrange.component';

@NgModule({
  imports: [
    CommonModule,
    GadgetRoutingModule
  ],
  declarations: [ArrangeComponent]
})
export class GadgetModule { }
