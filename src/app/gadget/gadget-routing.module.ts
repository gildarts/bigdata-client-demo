import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ArrangeComponent } from './arrange.component';

const routes: Routes = [
  {path: '', component: ArrangeComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GadgetRoutingModule { }
