import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PersonalRoutingModule } from './personal-routing.module';
import { ProfileComponent } from './profile.component';

@NgModule({
  imports: [
    CommonModule,
    PersonalRoutingModule
  ],
  declarations: [ProfileComponent]
})
export class PersonalModule { }
