import { UserService } from './user.service';
import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule
  ],
  declarations: [],
  providers: [UserService]
})
export class CoreModule {

  constructor(
    /** 選擇性的 DI，沒有載到也沒關系。 */
    @Optional()
    /** 略過此模組的 Injector，從 Parent 模組的 Injector 找。 */
    @SkipSelf()
    parentModule: CoreModule) {
    if (parentModule) {
      throw new Error(
        'CoreModule 已經載入，這個模組只能載入到 AppModule。');
    }
  }
}
