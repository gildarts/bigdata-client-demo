# NtpcBigdataClient

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.6.3.

## 環境需求
* Angular CLI v1.6.3 以上。
* VSCode 最新版。

## 安裝
* 執行 `yarn`

## 啟動 Debug Client
* 執行 `yarn start`

## 調整 API 轉 Port
* 設定檔：`proxy.config.json`